# Imports the connection handler package from the netmiko library
from netmiko import ConnectHandler
from getpass import getpass

import subprocess 
username = raw_input('Username: ')
password = getpass.getpass('Password: ')

# Stores the device attributes as an object called 'CSR'
CSR = {
    'device_type': 'cisco_ios',
    'ip': '192.168.1.220',
    'username': username,
    'password': password
}

# Establishes the SSH connection to the object defined above
net_connect = ConnectHandler(**CSR)

# Sends the command defined on line 22 to each device listed in 'devices.txt
# Prints output 
output = net_connect.send_command('show ip int brief')
print (output)

# Closes the connection
net_connect.disconnect()
