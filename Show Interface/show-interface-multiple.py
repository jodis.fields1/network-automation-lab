# Imports the connection handler package from the netmiko library
from netmiko import ConnectHandler
from getpass import getpass

import subprocess 
username = raw_input('Username: ')
password = getpass.getpass('Password: ')

# Uses 'devices.txt' to substitute the device IP defined on line 11
with open('devices.txt') as routers:
    for IP in routers:
        
        # Stores the device attributes as an object called 'Router'
        Router = {
            'device_type': 'cisco_ios',
            'ip': IP,
            'username': username,
            'password': password
        }
        
        # Establishes the SSH connection to each device listed in 'devices.txt'
        net_connect = ConnectHandler(**Router)

        # Sends the command defined on line 22 to each device listed in 'devices.txt
        # Separates the output of each device with 79 dashes (ref lines 22 + 26)
        print ('Connecting to ' + IP)
        print('-'*79)
        output = net_connect.send_command('sh ip int brief')
        print(output)
        print()
        print('-'*79)

# Closes the connection
net_connect.disconnect()




