# Imports the connection handler package from the netmiko library
from netmiko import ConnectHandler
from getpass import getpass

import subprocess 
username = raw_input('Username: ')
password = getpass.getpass('Password: ')

# Uses 'devices.txt' to substitute the device IP defined on line 10
with open('devices.txt') as routers:
    for IP in routers:

        # Stores the device attributes as an object called 'Router'
        Router = {
            'device_type': 'cisco_ios',
            'ip': IP,       # Substitutes device IP for the values in 'devices.txt'
            'username': username,
            'password': password
        }

        # Establishes the SSH connection to each device listed in 'devices.txt'
        net_connect = ConnectHandler(**Router)

        # Discovers the hostname and save it as a variable 
        hostname = net_connect.send_command('show run | i host')
        hostname.split(" ")
        hostname,device = hostname.split(" ")
        print ("Backing up " + device)

        # Saves the running config as 'hostname.txt' in a folder called 'backups' 
        filename = '/backups/' + device + '.txt'

        # Defines variables for simplicity
        showrun = net_connect.send_command('show run')
        showvlan = net_connect.send_command('show vlan')
        showver = net_connect.send_command('show ver')
        
        # Prints the output of the commands to the file 
        log_file = open(filename, "a")   
        log_file.write(showrun)
        log_file.write("\n")
        log_file.write(showvlan)
        log_file.write("\n")
        log_file.write(showver)
        log_file.write("\n")

# Closes the connection
net_connect.disconnect()
